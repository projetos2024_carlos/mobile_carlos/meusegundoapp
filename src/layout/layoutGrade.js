import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default function LayoutGrade() {
   return (
      <View style={styles.container}>
         <View style={styles.linha}>
            <View style={styles.box1}></View>
            <View style={styles.box2}></View>
         </View>
         <View style={styles.linha}>
            <View style={styles.box3}></View>
            <View style={styles.box4}></View>
         </View>
      </View>
   );
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
   },
   linha:{
      flexDirection: "row"
   },

   box1: {
      height: 100,
      width: 100,
      backgroundColor: "red"
   },
   box2: {
      height: 100,
      width: 100,
      backgroundColor: "green"
   },
   box3: {
      height: 100,
      width: 100,
      backgroundColor: "blue"
   },
   box4: {
      height: 100,
      width: 100,
      backgroundColor: "yellow"
   },
});
