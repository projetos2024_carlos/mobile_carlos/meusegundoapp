import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default function Layout() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text>
          MENU
        </Text>
      </View >
      <View style={styles.content}>
      <Text>
          CONTEUDO
        </Text>
      </View>
      <View style={styles.footer}>
        <Text>
          RODAPE
        </Text>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: 'space-between',
  },
  header: {
    heigth: 50,
    backgroundColor: "lightblue",
  },
  content: {
    flex: 1,
    backgroundColor: "lightgreen",    
  },
  footer: {
    heigth: 50,
    backgroundColor: "red",
  },
  text: {
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "right"
    
  },
});
