
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { Button } from 'react-native';


function TelaInicial({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>
        Tela Inicial
      </Text>
      <Button
        title="Ir para a segunda página" onPress={() => navigation.navigate("SegundaTela")}>
      </Button>

      <Button
        title="Ir para o menu" onPress={() => navigation.navigate("Menu")}>
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
 
   },
 
 
 });
 export default TelaInicial;