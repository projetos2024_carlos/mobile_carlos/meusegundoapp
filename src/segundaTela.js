
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { Button } from 'react-native';


function SegundaTela({ navigation }) {
   return (
     <View style={styles.container}>
       <Text>
         Tela Inicial
       </Text>
       <Button
         title="Voltar para home"
         onPress={() => navigation.goBack()}>
       </Button>
     </View>
   );
 }
 const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
 
   },
 
 
 });
 
 export default SegundaTela;